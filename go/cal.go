package main

import (
	"fmt"
	"io"
	"os"
	"time"
)

func main() {
	const KRED = "\x1B[31;1;4m"
	const KGRN = "\x1B[32m"
	const KREDU = "\x1B[31;1;4m"
	const KNRM = "\x1B[0m"
	//printf(KRED + "Hello world" + KNRM)

	now := time.Now()
	year := now.Year()
	dayofmonth := now.Day()
	var february_days = 0
	if (year%400) == 0 || ((year%100) != 0 && (year%4) == 0) {
		february_days = 29
	} else {
		february_days = 28
	}

	month := [12]int{31, february_days, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}

	monthname := [12]string{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
	days_in_month := month[now.Month()-1]

	printf(fmt.Sprintf("    %s - %d\n", monthname[now.Month()-1], year))
	printf(KREDU + " Mo Tu We Th Fr Sa Su" + KNRM + "\n")

	for x := 1; x < int(now.Weekday()); x++ {
		printf("   ")
	}

	for i := 1; i <= days_in_month; i++ {

		if dayofmonth == i {
			printf(KGRN + fmt.Sprintf("%3d", i) + KNRM)
		} else {
			printf(fmt.Sprintf("%3d", i))
		}
		if (i+int(now.Weekday())-1)%7 == 0 {
			printf("\n")
		}

	}
	printf("\n")
}

func printf(value string) {
	io.WriteString(os.Stdout, value)
}
